/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regan;

/**
 *
 * @author tim
 */
import  java.lang.Math;
//import  java.lang.*;

public class calcCls {

    public int no = 0;
    public double[] Ex = new double[5];
    public double[] Ey = new double[5];
    public double[] Exy = new double[5];
    public double[] Ex2 = new double[5];
    public double[] Ey2 = new double[5];
  
    public double[] a = new double[5];
    public double[] b = new double[5];
    public double[] r = new double[5];
    
    public String sLinearEq = "Linear  Y=";
    public String sLinearR = "R=";
    public String sExpEq = "Exp  Y=";
    public String sExpR = "R=";
    public String sLogEq = "Log  Y=";
    public String sLogR = "R=";
    public String sPowerEq = "Power  Y=";
    public String sPowerR = "R=";
    
    public void calcit() {
      try {     

//        System.out.println("calcCls:" + no);
        for(int i=1; i < 5; i++){
            System.out.println(String.valueOf(i) + "*" + String.valueOf(Ex[i]));
            b[i] = (no * Exy[i] - Ex[i] * Ey[i]) / (no * Ex2[i] - (Ex[i]*Ex[i]) );
            a[i] = (Ey[i] - b[i] * Ex[i]) / no;
            r[i] = (no * Exy[i] - Ex[i] * Ey[i]) / (Math.sqrt((no * Ex2[i] - (Ex[i]*Ex[i]) ) * (no * Ey2[i] - (Ey[i]*Ey[i]) ) ));
        }
        
        a[2] = Math.exp(a[2]);
        a[4] = Math.exp(a[4]); 
        
        //sLinearEq = "Linear  Y=" + String.format("%4.5f", a[1]) + " +" + String.format("%4.5f", b[1]) + " * X";
        sLinearEq = "Linear  Y=" + String.valueOf(a[1]) + " +" + String.valueOf(b[1]) + " * X";
        sLinearR = "R=" + String.format("%4.5f", r[1]);
        
        sExpEq = "Exp  Y=" + String.valueOf(a[2]) + " * e ^" + String.valueOf(b[2]) + " * X";
        sExpR = "R=" + String.format("%4.5f", r[2]);

        sLogEq = "Log  Y=" + String.valueOf( a[3]) + " +" + String.valueOf(b[3]) + " * LOG(X)";
        sLogR = "R=" + String.format("%4.5f", r[3]);

        sPowerEq = "Power  Y=" + String.valueOf(a[4]) + " * X ^" + String.valueOf(b[4]);
        sPowerR = "R=" + String.format("%4.5f", r[4]);
      } catch (IndexOutOfBoundsException e) {
            System.err.println("IndexOutOfBoundsException: " + e.getMessage());
      }
    } // calcit
    
} // calcCls
