/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regan;

import javax.swing.*;
import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.*;

/**
 *
 * @author Master
 */
public class ReganJFrame extends javax.swing.JFrame {

    private String sFn = ""; // CSV File

    public double[] x = new double[5];
    public double[] y = new double[5];
    public double[] a = new double[5];
    public double[] b = new double[5];
    public double[] r = new double[5];
    public double[] chtX = new double[5];
    public double[] chtY = new double[5];

    public double[] Ex = new double[5];
    public double[] Ey = new double[5];
    public double[] Exy = new double[5];
    public double[] Ex2 = new double[5];
    public double[] Ey2 = new double[5];
    
    private int no = 0, n = 0;
    private double m = 0;
    private double Ymin = 0, Ymax = 0;    
    
    /**
     * Creates new form ReganJFrame
     */
    public ReganJFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblLinear = new javax.swing.JLabel();
        lblExp = new javax.swing.JLabel();
        lblLog = new javax.swing.JLabel();
        lblPower = new javax.swing.JLabel();
        lblLinearR = new javax.swing.JLabel();
        lblExpR = new javax.swing.JLabel();
        lblLogR = new javax.swing.JLabel();
        lblPowerR = new javax.swing.JLabel();
        btnCalcValues = new javax.swing.JButton();
        lblInfo = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuSaveIt = new javax.swing.JMenuItem();
        mnuKeyboard = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItemExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mnuPredictX = new javax.swing.JMenuItem();
        mnuPredictY = new javax.swing.JMenuItem();
        jMenuAbout = new javax.swing.JMenu();
        jMenuItem6 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        lblLinear.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblLinear.setText("Linear Y=A + B * X");

        lblExp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblExp.setForeground(new java.awt.Color(51, 0, 255));
        lblExp.setText("Exp  Y=A * e^(B * X)");

        lblLog.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblLog.setForeground(new java.awt.Color(102, 0, 102));
        lblLog.setText("Log  Y=A + B * LOG(X)");

        lblPower.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPower.setForeground(new java.awt.Color(204, 0, 0));
        lblPower.setText("Power Y=A * X ^B");

        lblLinearR.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblLinearR.setText("R=");

        lblExpR.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblExpR.setForeground(new java.awt.Color(51, 51, 255));
        lblExpR.setText("R=");

        lblLogR.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblLogR.setForeground(new java.awt.Color(102, 0, 102));
        lblLogR.setText("R=");

        lblPowerR.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPowerR.setForeground(new java.awt.Color(204, 0, 0));
        lblPowerR.setText("R=");

        btnCalcValues.setText("Calculate Values");
        btnCalcValues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcValuesActionPerformed(evt);
            }
        });

        lblInfo.setText("Please select a CSV file or enter X & Y points via the keyboard");

        jMenu1.setText("File");

        jMenuItem1.setText("Open CSV File");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        
        mnuKeyboard.setText("Keyboard Input");
        mnuKeyboard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuKeyboardActionPerformed(evt);
            }
        });
        jMenu1.add(mnuKeyboard);


        jMenuSaveIt.setText("Save Results");
        jMenuSaveIt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	jMenuSaveItActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuSaveIt);        
        
        jMenu1.add(jSeparator1);

        jMenuItemExit.setText("Exit");
        jMenuItemExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemExitActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Predict");

        mnuPredictX.setText("Predict X value");
        mnuPredictX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuPredictXActionPerformed(evt);
            }
        });
        jMenu2.add(mnuPredictX);

        mnuPredictY.setText("Predict Y value");
        mnuPredictY.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuPredictYActionPerformed(evt);
            }
        });
        jMenu2.add(mnuPredictY);

        jMenuBar1.add(jMenu2);

        jMenuAbout.setText("Help");

        jMenuItem6.setText("About");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenuAbout.add(jMenuItem6);

        jMenuBar1.add(jMenuAbout);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblExp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblLog, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPower, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblLinear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblLogR)
                    .addComponent(lblPowerR)
                    .addComponent(lblExpR)
                    .addComponent(lblLinearR))
                .addGap(65, 65, 65))
            .addGroup(layout.createSequentialGroup()
                .addGap(218, 218, 218)
                .addComponent(btnCalcValues)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 621, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 12, Short.MAX_VALUE)
                .addComponent(lblInfo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblLinear)
                    .addComponent(lblLinearR))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblExp)
                    .addComponent(lblExpR))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblLog)
                    .addComponent(lblLogR))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPower)
                    .addComponent(lblPowerR))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCalcValues)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        this.sFn = this.getFileName();
       // JOptionPane.showMessageDialog(null, sFn);
        lblInfo.setText("File Loaded: " + this.sFn);
    }//GEN-LAST:event_jMenuItem1ActionPerformed


    private void jMenuSaveItActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        BufferedWriter writer = null; // Note outside try block
        try {
            //create a output file
            String results = "results.txt";
            File logFile = new File(results);

            // This will output the full path where the file will be written to...
           // System.out.println(logFile.getCanonicalPath());
            
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(lblLinear.getText() + "  " + lblLinearR.getText());
            writer.newLine();
            writer.write(lblExp.getText() + "  " + lblExpR.getText());
            writer.newLine();
            writer.write(lblLog.getText() + "  " + lblLogR.getText());
            writer.newLine(); 
            writer.write(lblPower.getText() + "  " + lblPowerR.getText());
            writer.newLine();
            
            JOptionPane.showMessageDialog(null, "Saved: " + results);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
        
    }//GEN-LAST:event_jMenuSaveItActionPerformed
    
    
    private void jMenuItemExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemExitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItemExitActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        JOptionPane.showMessageDialog(null, "Regan is a Linear regression analysis (curve fitting) program, used to reduce simple X and Y data\n" +
                                "information to a formula. The Program compairs the fit of linear, log, exponential, and power;\ncalculating formula and coefficient for all four.\n", "Regan v4", JOptionPane.PLAIN_MESSAGE);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void btnCalcValuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcValuesActionPerformed
      //  JOptionPane.showMessageDialog(null, sFn);
      
        try {
        BufferedReader CSVFile = new BufferedReader(new FileReader(sFn));

      String dataRow = CSVFile.readLine();
      // Read the number of the lines in .csv file 
      // i = row of the .csv file
      int i = 0; 
      while (dataRow != null){
          i++;
          dataRow = CSVFile.readLine();
        }
      System.out.println(i);
      no = i; // Number of lines / data items in the CSV
      CSVFile.close(); // Close the file once all data has been read.

      double[] xa = new double[i];
      double[] ya = new double[i];
      
       // Read first line.
      // The while checks to see if the data is null. If 
      // it is, we've hit the end of the file. If not, 
      // process the data.
      i = 0;

      // Read the file again to save the data into arrays
      BufferedReader CSV = new BufferedReader(new FileReader(sFn));

      String data = CSV.readLine();

      while (data != null){
          String[] dataArray = data.split(",");
          for (String item:dataArray) {
            xa[i] = Double.parseDouble(dataArray[0]);
            ya[i] = Double.parseDouble(dataArray[1]);
            } // for
          
          x[1] =  xa[i];
          y[1] = ya[i];
          inpcalc();
          //System.out.print(address[i] + "\n"); 
          data = CSV.readLine(); // Read next line of data.
          i++;
      } // while
      
      calcValues();
      
     } catch (IndexOutOfBoundsException e) {
        System.err.println("IndexOutOfBoundsException: " + e.getMessage());
     } catch (IOException e) {
        System.err.println("Caught IOException: " + e.getMessage());
     } // try
        
    }//GEN-LAST:event_btnCalcValuesActionPerformed

    private void mnuPredictYActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuPredictYActionPerformed
        predictY();
    }//GEN-LAST:event_mnuPredictYActionPerformed

    private void mnuPredictXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuPredictXActionPerformed
        predictX();
        // String test1= JOptionPane.showInputDialog(this, "Predict X when Y =", "Top Title", JOptionPane.QUESTION_MESSAGE);
    }//GEN-LAST:event_mnuPredictXActionPerformed

    private void mnuKeyboardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuKeyboardActionPerformed
        keyboardInput();
    }//GEN-LAST:event_mnuKeyboardActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReganJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReganJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReganJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReganJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ReganJFrame().setVisible(true);
            }
        });
    }
    
    
    private void calcValues() {
      calcCls mycalcit = new calcCls();
      mycalcit.Ex = Ex;
      mycalcit.Ex2 = Ex2;
      mycalcit.Exy = Exy;
      mycalcit.Ey = Ey;
      mycalcit.Ey2 = Ey2;
      mycalcit.no = no;      
      mycalcit.calcit();
      a = mycalcit.a;
      b = mycalcit.b;
      r = mycalcit.r;
      //JOptionPane.showMessageDialog(null, mycalcit.sLinearEq);
      
      lblLinear.setText(mycalcit.sLinearEq);
      lblLinearR.setText(mycalcit.sLinearR);
      lblExp.setText(mycalcit.sExpEq);
      lblExpR.setText(mycalcit.sExpR);
      lblLog.setText(mycalcit.sLogEq);
      lblLogR.setText(mycalcit.sLogR);
      lblPower.setText(mycalcit.sPowerEq);
      lblPowerR.setText(mycalcit.sPowerR);    
    } // calcValues
    

    private void inpcalc() {        
        x[2] = x[1];
        y[2] = Math.log(y[1]);  //  exp
        x[3] = Math.log(x[1]);
        y[3] = y[1];           //  log
        x[4] = Math.log(x[1]);
        y[4] = Math.log(y[1]);  // power

        for(int i=1; i<5; i++){
          Exy[i] = Exy[i] + x[i] * y[i];
          Ex[i] = Ex[i] + x[i];
          Ey[i] = Ey[i] + y[i];
          Ex2[i] = Ex2[i] + (x[i]*x[i]);
          Ey2[i] = Ey2[i] + (y[i]*y[i]);
        }
    } // inpcalc
    
    
    private void predictY() {
        y[1] = Double.parseDouble(JOptionPane.showInputDialog(this, "Predict Y when X =", "When Y data is equal to...", JOptionPane.QUESTION_MESSAGE) );
        x[1] = (y[1] - a[1]) / b[1];
        x[2] = (Math.log(y[1] / a[2])) / b[2];
        x[3] = (y[1] - a[3]) / b[3];
        x[3] = Math.pow(Math.exp(1) , (x[3]) );
        x[4] = Math.pow( (y[1] / a[4]) , (1 / b[4]) );

        String mm = "When Y="+ String.valueOf(y[1]) + "\n\n";
        mm += "Linear  X=" + String.valueOf(x[1]) + "\n";
        mm += "Exp   X=" + String.valueOf(x[2]) + "\n";
        mm += "Log   X=" + String.valueOf(x[3]) + "\n";
        mm += "Power X=" + String.valueOf(x[4]);
        JOptionPane.showMessageDialog(null, mm);
    }
    
    private void predictX(){
        x[1] = Double.parseDouble(JOptionPane.showInputDialog(this, "Predict X when Y =", "When X data is equal to...", JOptionPane.QUESTION_MESSAGE) );
        y[1] = a[1] + b[1] * x[1];
        y[2] = a[2] * Math.pow( 2.718281828 , (b[2] * x[1]) );
        y[3] = a[3] + b[3] * Math.log(x[1]);
        y[4] = a[4] * Math.pow(x[1] , b[4]);

        String mm = "When X=" + String.valueOf(x[1]) + "\n\n";
        mm += "Linear  Y=" + String.valueOf(y[1]) + "\n";
        mm += "Exp   Y=" + String.valueOf(y[2]) + "\n";
        mm += "Log   Y=" + String.valueOf(y[3]) + "\n";
        mm += "Power Y=" + String.valueOf(y[4]);
        JOptionPane.showMessageDialog(null, mm);
    }
    
    private void keyboardInput() {
        no = Integer.parseInt(JOptionPane.showInputDialog(this, "Number of X and Y Data points", "No Points", JOptionPane.QUESTION_MESSAGE) );
        n=1;
        if (no <= 1) {
            System.exit(0);
        }

        while (n <= no) {
            x[1] = Double.parseDouble(JOptionPane.showInputDialog(this,"X Data","Data for X info: " + String.valueOf(n), JOptionPane.QUESTION_MESSAGE));
            y[1] = Double.parseDouble(JOptionPane.showInputDialog(this,"Y Data","Data for Y info: " + String.valueOf(n), JOptionPane.QUESTION_MESSAGE));
            n++;
            inpcalc();
        } // While
        
        calcValues();
    } // keyboardInput
    
    private String getFileName() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Open CSV Data File");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT & CSV Text Files", "txt", "csv");
        fileChooser.setFileFilter(filter);

        String sfileIn = "";
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            sfileIn = fileChooser.getSelectedFile().getPath(); 
        } else {
            System.out.println("Cancel Button pressed");
        }
        
        return sfileIn;
    } // getFileName
    
    private static void infoBox(String infoMessage, String titleBar) {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
    
    
    
    
    
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcValues;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenuAbout;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuSaveIt;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItemExit;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel lblExp;
    private javax.swing.JLabel lblExpR;
    private javax.swing.JLabel lblInfo;
    private javax.swing.JLabel lblLinear;
    private javax.swing.JLabel lblLinearR;
    private javax.swing.JLabel lblLog;
    private javax.swing.JLabel lblLogR;
    private javax.swing.JLabel lblPower;
    private javax.swing.JLabel lblPowerR;
    private javax.swing.JMenuItem mnuKeyboard;
    private javax.swing.JMenuItem mnuPredictX;
    private javax.swing.JMenuItem mnuPredictY;
    // End of variables declaration//GEN-END:variables
}
